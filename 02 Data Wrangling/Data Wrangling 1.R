require("tidyr")
require("dplyr")
require("jsonlite")
require("ggplot2")
require("extrafont")
require("RCurl")
require("reshape2")


# Creating the data frame needed from SQL database
NYC_CoD_df <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from NYC_Deaths"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_elg922', PASS='orcl_elg922', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE) ))



#Daniel's code for the first plot
col <- NYC_CoD_df %>% select(YEAR, SEX ,COUNT)
col2011 <- NYC_CoD_df %>% select(YEAR, SEX ,COUNT) %>% dplyr::filter(YEAR == "2011")
sumMale2011 <- col2011 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "MALE") %>% select(COUNT) %>% sum
sumFemale2011 <- col2011 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "FEMALE") %>% select(COUNT) %>% sum
col2010 <- NYC_CoD_df %>% select(YEAR, SEX ,COUNT) %>% dplyr::filter(YEAR == "2010")
sumMale2010 <- col2010 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "MALE") %>% select(COUNT) %>% sum
sumFemale2010 <- col2010 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "FEMALE") %>% select(COUNT) %>% sum
col2009 <- NYC_CoD_df %>% select(YEAR, SEX ,COUNT) %>% dplyr::filter(YEAR == "2009")
sumMale2009 <- col2009 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "MALE") %>% select(COUNT) %>% sum
sumFemale2009 <- col2009 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "FEMALE") %>% select(COUNT) %>% sum
col2008 <- NYC_CoD_df %>% select(YEAR, SEX ,COUNT) %>% dplyr::filter(YEAR == "2008")
sumMale2008 <- col2008 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "MALE") %>% select(COUNT) %>% sum
sumFemale2008 <- col2008 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "FEMALE") %>% select(COUNT) %>% sum
col2007 <- NYC_CoD_df %>% select(YEAR, SEX ,COUNT) %>% dplyr::filter(YEAR == "2007")
sumMale2007 <- col2007 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "MALE") %>% select(COUNT) %>% sum
sumFemale2007 <- col2007 %>%  select(SEX, COUNT) %>% dplyr::filter(SEX == "FEMALE") %>% select(COUNT) %>% sum
years <- c(2007, 2008, 2009, 2010, 2011)
Male <-c(sumMale2007, sumMale2008, sumMale2009, sumMale2010, sumMale2011)
Female <-c(sumFemale2007, sumFemale2008, sumFemale2009, sumFemale2010, sumFemale2011)
df1 <- data.frame(years, Male, Female)
mdf <- melt(df1, id.var="years")

ggplot(mdf, aes(x = years, y = value, fill = variable)) +
  geom_bar(stat = "identity") +
  labs(title='Total Number of Deaths per Year in New York City') +
  labs(x="Years", y=paste("Deaths"))



