require("tidyr")
require("dplyr")
require("jsonlite")
require("ggplot2")
require("extrafont")
require("RCurl")
require("reshape2")


# Creating the data frame needed from SQL database
NYC_CoD_df <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from NYC_Deaths"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_elg922', PASS='orcl_elg922', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE) ))



#Pablo's code for the third plot
totals <- NYC_CoD_df %>% select(YEAR, COD, COUNT) %>% arrange(YEAR, COD)
tot_2007 <- totals %>% filter(YEAR == "2007")
tot_2008 <- totals %>% filter(YEAR == "2008")
tot_2009 <- totals %>% filter(YEAR == "2009")
tot_2010 <- totals %>% filter(YEAR == "2010")
tot_2011 <- totals %>% filter(YEAR == "2011")
sum_heart_2007 <- tot_2007 %>% filter(COD == "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_notheart_2007 <- tot_2007 %>% filter(COD != "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_heart_2008 <- tot_2008 %>% filter(COD == "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_notheart_2008 <- tot_2008 %>% filter(COD != "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_heart_2009 <- tot_2009 %>% filter(COD == "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_notheart_2009 <- tot_2009 %>% filter(COD != "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_heart_2010 <- tot_2010 %>% filter(COD == "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_notheart_2010 <- tot_2010 %>% filter(COD != "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_heart_2011 <- tot_2011 %>% filter(COD == "DISEASES OF HEART") %>% select(COUNT) %>% sum
sum_notheart_2011 <- tot_2011 %>% filter(COD != "DISEASES OF HEART") %>% select(COUNT) %>% sum
years <- c(2007, 2008, 2009, 2010, 2011)
Heart_Disease <- c(sum_heart_2007, sum_heart_2008, sum_heart_2009, sum_heart_2010, sum_heart_2011)
Other_Causes <- c(sum_notheart_2007, sum_notheart_2008, sum_notheart_2009, sum_notheart_2010, sum_notheart_2011)
df_Counts <- data.frame(years, Heart_Disease, Other_Causes)
melted_Counts <- melt(df_Counts, id.vars = "years")

ggplot(melted_Counts, aes(x = years , y = value, fill = variable), theme_classic()) + geom_bar(stat = "identity", position = "dodge") + labs(title = "Number of Deaths per Year") + labs(y = paste("Deaths"))

